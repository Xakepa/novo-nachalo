const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    'o2hs123o6G0ol090le42H'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const secretMessage = gets().split('');
const filtered = [];

for (const symbol of secretMessage) {
    if (isNaN(symbol * 1)) {
        filtered.push(symbol)
    }
}
print(filtered.reverse().join(''));

// Ver 2.0
// let secretMessage = gets();
// secretMessage = secretMessage.replace(/[0-9]/g, '');
  
// print(secretMessage.split('').reverse().join(''));