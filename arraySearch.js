const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '4,3,2,7,8,2,3,1'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const inputArr = Array.from(gets().split(',').map(Number));
let missingNums = [];

for (let i = 1; i <= inputArr.length; i += 1) {
    missingNums.push(i)
}

const filtered = missingNums.filter((x, i, arr) => !inputArr.includes(x));
print(filtered.join(','));