const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '4 3',
    '3', 
    '4 5',
    '5 2', 
    '6 9',
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const [width, heigth] = gets().split(' ').map(Number);
const obstructions = +gets();
let breakPoint = 0;

for (let i = 1; i <= obstructions; i += 1) {
    let [a, b] = gets().split(' ').map(Number);
    if (width > a || heigth > b) {
        breakPoint = i;
        break;
    }
}
(breakPoint) ? print(breakPoint) : print('No crash');