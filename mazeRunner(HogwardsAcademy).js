const turns = +gets();

const myPowerfullSpell = (numb) => {
    const currentNumber = numb.split('');
    let sumOdd = 0;
    let sumEven = 0;
    for (const digit of numb) {
        if (+digit % 2 === 1) {
            sumOdd += +digit;
        } else {
            sumEven += +digit;
        }
    }
    if (sumOdd > sumEven) {
        return 'right';
    } else if (sumEven > sumOdd) {
        return 'left';
    } else {
        return 'straight'
    }
}
for (let i = 1; i <= turns; i++) {
    const n = gets();
    print(myPowerfullSpell(n));
}