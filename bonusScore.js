const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '5'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

let score = +gets();

switch (score) {
    case 1:
    case 2:
    case 3:
        score *= 10; break;
    case 4:
    case 5:
    case 6:
        score *= 100; break;
    case 7:
    case 8:
    case 9:
        score *= 1000; break;
    default:
        score = 'invalid score'
}
print(score);