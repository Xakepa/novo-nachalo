const getGets = (arr) => {
    let index = 0

    return () => {
        const toReturn = arr[index]
        index += 1
        return toReturn
    }
}
// this is the test
const test = [
    '15'
]

const gets = this.gets || getGets(test)
const print = this.print || console.log

let startIndex = 1;
const n = +gets();
let result = '';

while (startIndex <= n) {
    result += startIndex + ' ';
    startIndex++;
}
print(result);
