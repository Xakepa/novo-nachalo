const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '43691'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

'use strict';

let dec = +gets();
//alternative print(dec.toString(2));
let bin = '';

do {
    bin = (dec & 1) + bin;
    dec >>= 1;
} while (dec > 0)

print(bin);