const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
  '-7231',
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

let n = gets();
let sum = 0;

while (n.length > 1) {
    n = n.toString().split('').map(Number);
    for (let i = 0; i < n.length; i++) {
        if (!isNaN(n[i])) {
            sum += n[i];
        }
    }
    n = sum.toString();
    sum = 0;
}
print(n);
