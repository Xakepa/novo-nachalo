const [a, b, c] = gets().split('').map(Number);

const allCombinations = [(a + b * c), (a * b + c), (a * b * c), (a + b + c)];
print(Math.max(...allCombinations));