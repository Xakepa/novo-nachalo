const input = gets().split('');
const filtered = input.filter((x, i, arr) => x !== arr[i + 1]);
print(filtered.join(''));