const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '10',
    '2',
    '3',
    '-6',
    '-1',
    '2',
    '-1',
    '6',
    '4',
    '-8',
    '8'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

let n = +gets();
let arr = [];

for (let i = 0; i < n; i++) {
    arr.push(+gets());
}

let currentSum = 0;
let maxSum = 0;

for (let i = 0; i < arr.length; i++) {
    currentSum += arr[i];
    if (currentSum > 0) {
        if (currentSum > maxSum) {
            maxSum = currentSum;
        }
    } else {
        currentSum = 0;
    }
}

print(maxSum);