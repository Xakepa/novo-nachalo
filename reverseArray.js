const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '1 2 3 4 5 6 7'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const arr = gets().split(' ').map(Number);
const reversed = [];

for (let i = 0; i < arr.length; i++) {
    let j = arr.length - 1 - i
    reversed[j] = arr[i];
}
print(reversed);