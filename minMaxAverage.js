const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
  '3',
   '2',
   '5',
   '1'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const numbersSize = +gets();
const numbers = Array.from({
    length: numbersSize
}).map(() => +gets());

const min = Math.min(...numbers);
const max = Math.max(...numbers);
const sum = numbers.reduce((acc, totalSum) => acc + totalSum, 0)
const average = sum / numbersSize;
print(`min=${min.toFixed(2)}\nmax=${max.toFixed(2)}\nsum=${sum.toFixed(2)}\navg=${average.toFixed(2)}`);