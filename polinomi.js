const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
  '3',
  '5 0 1',
  '7 4 -3', 	
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const arrsUnits = +gets();
const polynomial = gets().split(' ').map(Number);
const secondPolynomial = gets().split(' ').map(Number);

const coefs = polynomial.map((x, i) => x + secondPolynomial[i]);
print(coefs.join(' '));