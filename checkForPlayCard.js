const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '500'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const checkString = gets();

switch (checkString) {
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
    case '10':
    case 'J':
    case 'Q':
    case 'K':
    case 'A':
        print(`yes ${checkString}`)
        break;
    default:
        print(`no ${checkString}`)
}
