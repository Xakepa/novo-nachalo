const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '500'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const input = +gets();
let counter = 0;
for (let i = 5; i <= input; i += 5) {
    let fact = i;
    while (fact % 5 === 0) {
        fact /= 5;
        counter++
    }
}
print(counter);
