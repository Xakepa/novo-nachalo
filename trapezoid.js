const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '10'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const size = +gets();

print('.'.repeat(size) + '*'.repeat(size));

for (let i = 1; i <= size - 1; i += 1) {
    print('.'.repeat(size - i) + '*' + '.'.repeat(size - 2 + i) + '*');
}
print('*'.repeat(size * 2));