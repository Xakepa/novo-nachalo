const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '5',
    '11',
    '22',
    '11',
    '22',
    '11',
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const numsCount = gets().split(' ').map(Number);
const nums = Array.from({
    length: numsCount
}).map(() => +gets());

const mergedNums = [];
const squashedNums = [];

for (let i = 0; i < numsCount - 1; i += 1) {
    const currentNum = nums[i].toString();
    const nextNum = nums[i + 1].toString();
    const merged = currentNum[1] + nextNum[0];
    mergedNums.push(merged);
    let middleSquashing = +currentNum[1] + +nextNum[0];
    if (middleSquashing > 9) {
        middleSquashing %= 10;
    }
    const squashed = currentNum[0] + middleSquashing + nextNum[1];
    squashedNums.push(squashed);
}
print(mergedNums.join(' '));
print(squashedNums.join(' '));