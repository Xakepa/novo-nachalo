const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '4',
    '3',
    '5',
    '4',
    '4',
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

let first = +gets();
const second = +gets();
const third = +gets();
const forth = +gets();
const fifth = +gets();

if (first < second) {
    first = second;
}
if (first < third) {
    first = third;
}
if (first < forth) {
    first = forth;
}
if (first < fifth) {
    first = fifth;
}
print(first);

//ver 2.0
// print(Math.max(first, second, third, forth, fifth));