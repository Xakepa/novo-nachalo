'use strict';
const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '10'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const n = +gets();
let pyramid = '';

for (let i = 1; i <= n; i++) {
    pyramid += i + ' ';
    print(pyramid)
}

let bottom = '';

for (let z = n - 1; z >= 0; z -= 1) {

    for (let k = 1; k <= z; k += 1) {
        bottom += k + ' ';
    }
    print(bottom);
    bottom = '';
}