const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    'Fun exam right'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const words = gets().split(' ');
const longestWord = Math.max(...(words.map(el => el.length)));
let extracted = '';

for (let i = 0; i < longestWord; i++) {

    words.forEach(word => {
        extracted += word.substring(word.length - (1 + i), word.length - i);
    });
};

extracted = extracted.split('');

for (let i = 0; i < extracted.length; i++) {

    const nPosition = extracted[i].toLowerCase().charCodeAt(0) - 96;
    let move = nPosition % extracted.length + i;
    if (move >= extracted.length) {
        move = Math.abs(extracted.length - move);
    }
      
    const removed = extracted.splice(i, 1).join('');
    extracted.splice(move, 0, removed);
}
print(extracted.join(''));
