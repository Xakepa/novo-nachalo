'use strict';

const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '1337'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

let sheets = +gets();
const arr = [1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1];
const unused = [];

for (const sheet of arr) {
    if (sheets >= sheet) {
        sheets -= sheet
    } else {
        unused.push(sheet)
    }
}

for (let i = 0; i < unused.length; i++) {
    switch (unused[i]) {
        case 1024:
            print('A0')
            break;
        case 512:
            print('A1')
            break;
        case 256:
            print('A2')
            break;
        case 128:
            print('A3')
            break;
        case 64:
            print('A4')
            break;
        case 32:
            print('A5')
            break;
        case 16:
            print('A6')
            break;
        case 8:
            print('A7')
            break;
        case 4:
            print('A8')
            break;
        case 2:
            print('A9')
            break;
        case 1:
            print('A10')
            break;
    }
}

