const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '30',
    '42',
    '70',
    '35',
    '90',
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const first = +gets();
const second = +gets();
const third = +gets();
const forth = +gets();
const fifth = +gets();
let divided = 0;
let result;

for (let i = 2; i < i + 1; i += 1) {
    divided = 0;
    if (!(i % first)) {
        divided += 1;
    }
    if (!(i % second)) {
        divided += 1;
    }
    if (!(i % third)) {
        divided += 1;
    }
    if (!(i % forth)) {
        divided += 1;
    }
    if (!(i % fifth)) {
        divided += 1;
    }
    if (divided >= 3) {
        result = i;
        console.log(divided);
        break;
    }
}
print(result);