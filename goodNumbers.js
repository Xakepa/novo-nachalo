const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '256 768'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const [startIndex, endIndex] = gets().split(' ').map(Number);

const isGoodNumber = (num) => {
    
    const splited = num.toString().split('').map(Number)
    const divisors = splited.filter((divisor => divisor !== 0));
    let isGood = true;
    for (const divisor of divisors) {
        if (num % divisor) {    // if num === 1 true-like result
            isGood = false;
            break;
        }
    }
    return isGood;
}

let goodNumbersCount = 0;

for (let i = startIndex; i <= endIndex; i += 1) {
    if (isGoodNumber(i)) {
        goodNumbersCount += 1;
    }
}
print(goodNumbersCount);