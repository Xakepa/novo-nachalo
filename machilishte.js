const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '5 3 4 5 5'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const evals = gets().split(' ').map(Number);
const noPrize = evals.includes(2);
const stars = evals.filter(e => e === 6);

(noPrize || stars.length === 0) ? print('No') : print('*'.repeat(stars.length));