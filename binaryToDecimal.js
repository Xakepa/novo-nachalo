const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '111001'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const binary = gets()
// print(parseInt(binary, 2)); built in functionality

let result = 0;

for (let i = 0, j = binary.length - 1; i < binary.length; i += 1, j -= 1) {
    result += binary[i] * Math.pow(2, [j])
}

print(result);