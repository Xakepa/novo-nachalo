const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '110100010101100111100010011010101111001101111'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

let bin = gets()
let splited = '';

while (bin.length % 4 !== 0) {
    bin = '0' + bin;
};

for (let i = 0; i < bin.length; i += 1) {
    splited += bin[i]
    if ((i + 1) % 4 === 0) {
        splited += ' ';
    }
};

const bits = splited.split(' ');
let hex = ''

for (const bit of bits) {
    
    switch (bit) {
        case '0000':
            hex += 0;
            break;
        case '0001':
            hex += 1;
            break;
        case '0010':
            hex += 2;
            break;
        case '0011':
            hex += 3;
            break;
        case '0100':
            hex += 4;
            break;
        case '0101':
            hex += 5;
            break;
        case '0110':
            hex += 6;
            break;
        case '0111':
            hex += 7;
            break;
        case '1000':
            hex += 8;
            break;
        case '1001':
            hex += 9;
            break;
        case '1010':
            hex += 'A';
            break;
        case '1011':
            hex += 'B';
            break;
        case '1100':
            hex += 'C';
            break;
        case '1101':
            hex += 'D';
            break;
        case '1110':
            hex += 'E';
            break;
        case '1111':
            hex += 'F'
    }
}
print(hex);