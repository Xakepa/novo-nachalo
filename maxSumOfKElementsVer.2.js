let n = +gets();
let k = +gets();

const elements = Array.from({length: n}).map(() => +gets());
const sortedAndReversed = elements.sort((a, b) => (a - b)).reverse();

let sum = 0;
for (let x = 0; x < k; x++) {
    sum += sortedAndReversed[x];
}
print(sum);