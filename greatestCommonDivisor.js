'use strict';
const [firstNum, secondNum] = gets().split(' ').map(Number);

const gcd = (a, b) => {
    while (b) {
        let temp = b;   
        b = a % b;      
        a = temp;
    }
    return a;
}
print(gcd(firstNum, secondNum));