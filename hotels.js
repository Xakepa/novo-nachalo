'use strict';
const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '5', 
    '1 2 3 4 5'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const hotels = +gets();
const heights = Array.from(gets().split(' ').map(Number));
const otherSide = heights.slice().reverse();
let seen = 0;
let max = 0;

for (let i = 0; i < heights.length; i++) {
    if (max < heights[i]) {
        max = heights[i]
        seen += 1;
    }
}

let maxOtherSide = 0;
let otherSideSeen = 0;

for (let i = 0; i < otherSide.length; i++) {
    if (maxOtherSide < otherSide[i]) {
        maxOtherSide = otherSide[i]
        otherSideSeen += 1;
    }
}
print(seen, otherSideSeen);