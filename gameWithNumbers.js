const [players, loniNumber] = gets().split(' ').map(Number);
const playersNumbers = gets().split(' ').map(Number);
const scores = [];

const equalDivisors = (x) => {
    let score = 0;

    for (let i = 2; i <= loniNumber; i++) {
        if (x !== 0) {
            if (x % i === 0 && loniNumber % i === 0) {
                score += 1;
            }
        }
    }
    return score;
}

for (const num of playersNumbers) {
    scores.push(equalDivisors(num))
}
const max = Math.max(...scores)
const positions = [];

for (let i = 0; i < playersNumbers.length; i++) {
    if (equalDivisors(playersNumbers[i]) === max) {
        positions.push(i + 1);
    }
}
if (max) {
    print(max);
    print(positions.join(' '));
} else {
    print('No winners');
};