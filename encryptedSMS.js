const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    'happy'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const word = gets();

let previous = word.slice(0, 2) + word[0];
let encrypted = previous;

for (let i = 2; i < word.length; i++) {
    encrypted += word[i] + previous;
    previous = encrypted;
};

(word.length === 1) ? print(word) : print(encrypted);