const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '4'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const n = +gets();
const size = 2 * n - 1;
const defaultPath = Array.from({
    length: n - 1
}).map((x) => x = '.');

let direction;
let move = 0;

for (let i = 0; i < size; i += 1) {


    if (move === n - 1) {
        direction = 'backward'
    } else if (move === 0) {
        direction = 'forward'
    }
     
    defaultPath.splice(move, 0, '*');
    print(defaultPath.join(''));
    defaultPath.splice(move, 1);

    if (direction === 'forward') {
        move++;
    } else {
        move--;
    }
}
