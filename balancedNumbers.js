const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '275',
    '693',
    '110',
    '742',
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;


const balanced = (num) => {
    num = num.toString().split('').map(Number)
    if (num[0] + num[2] === num[1]) {
        return true;
    } else {
        return false;
    }
}
let sum = 0;
let n = +gets();

while (balanced(n)) {
    sum += n;
    n = +gets();
}
print(sum);