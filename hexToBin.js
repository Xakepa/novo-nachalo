const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '1A2B3C4D5E6F'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const hex = gets();
let bin = '';

for (const char of hex) {
    switch (char) {
        case '0':
            bin += '0000';
            break;
        case '1':
            bin += '0001';
            break;
        case '2':
            bin += '0010';
            break;
        case '3':
            bin += '0011';
            break;
        case '4':
            bin += '0100';
            break;
        case '5':
            bin += '0101';
            break;
        case '6':
            bin += '0110';
            break;
        case '7':
            bin += '0111';
            break;
        case '8':
            bin += '1000';
            break;
        case '9':
            bin += '1001';
            break;
        case 'A':
            bin += '1010';
            break;
        case 'B':
            bin += '1011';
            break;
        case 'C':
            bin += '1100';
            break;
        case 'D':
            bin += '1101';
            break;
        case 'E':
            bin += '1110';
            break;
        case 'F':
            bin += '1111';
    }
}

let binSplited = bin.split('');

for (var i = 0; i < binSplited.length; i++) {
    if (binSplited[i] === '1') {
        bin = bin.slice(i);
        break;
    }
}
print(bin)

// option 2 slower
// while (binSplited[0] === '0') {
//     binSplited.shift();
// }
// print(binSplited.join(''));



