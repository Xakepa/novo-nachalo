const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '1',
    '2',
    '3',
    '4',
    '2',
    '8',
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

let [q1, q2, q3, q4] = Array.from({
    length: 4
}).map(() => +gets());
const row = +gets();
let col = +gets()
let currentRow = [q1, q2, q3, q4];

    for (var k = 4; k < col; k++) {
       
        const quadronacci = q1 + q2 + q3 + q4;
        q1 = q2;
        q2 = q3;
        q3 = q4;
        q4 = quadronacci;

        currentRow.push(q4);
    }
print(currentRow.join(' '));

for (var rows = 1; rows < row; rows++) {
    currentRow = [];
    for (let k = 0; k < col; k++) {

        const quadronacci = q1 + q2 + q3 + q4;
        q1 = q2;
        q2 = q3;
        q3 = q4;
        q4 = quadronacci;

        currentRow.push(q4);
    }
    print(currentRow.join(' '));
};