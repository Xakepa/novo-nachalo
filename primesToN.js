const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '2'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const N = +gets();
const primes = [];

for (let i = 1; i <= N; i++) {
    let prime = true;

    for (let x = 2; x <= Math.sqrt(i); x++) {
        if (!(i % x)) {
            prime = false;
            break;
        }
    }
    if (prime) {
        primes.push(i)
    }
}
print(primes.join(' '));