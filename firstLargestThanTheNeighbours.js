const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '6',
    '-26 -27 -28 -31 -35 -37'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const arrSize = +gets();
const theArray = gets().split(' ').map(Number);
const result = theArray.find((x, i, arr) => x > arr[i + 1] && x > arr[i - 1]);
print(theArray.indexOf(result));