const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '8',
'28 6 21 6 -7856 73 73 -56',
'73' 	
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const arrSize = +gets();
const theArray = Array.from(gets().split(' ').map(Number));
const findThis = +gets();

const filtered = theArray.filter(x => x === findThis);
print(filtered.length);


// with Reduce
// const countThis = theArray.reduce((acc, currentValue) => {
//     if (currentValue === findThis) {
//         acc += 1;
//        }
//        return acc;
//    }, 0);
   
//    print(countThis);