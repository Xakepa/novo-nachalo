const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '2004',
    '12',
    '10'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

let year = +gets();
let month = +gets() - 1;
let day = +gets() - 1;
let leaping = false;
const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
month = months[month];

if (!(year % 4)) {
    leaping = true; //високосна година
}

switch (true) {
    case (day === 0 && month === 'Jan'):
        year -= 1;
        month = 'Dec';
        day = 31;
        print(`${day}-${month}-${year}`);
        break;
    case (day === 0 && month === 'Dec'):
        month = 'Nov'
        day = 30;
        print(`${day}-${month}-${year}`);
        break;
    case (day === 0 && month === 'Nov'):
        month = 'Oct'
        day = 31;
        print(`${day}-${month}-${year}`);
        break;
    case (day === 0 && month === 'Oct'):
        month = 'Sep'
        day = 30;
        print(`${day}-${month}-${year}`);
        break;
    case (day === 0 && month === 'Sep'):
        month = 'Aug'
        day = 31;
        print(`${day}-${month}-${year}`);
        break;
    case (day === 0 && month === 'Aug'):
        month = 'Jul'
        day = 31;
        print(`${day}-${month}-${year}`);
        break;
    case (day === 0 && month === 'Jul'):
        month = 'Jun'
        day = 30;
        print(`${day}-${month}-${year}`);
        break;
    case (day === 0 && month === 'Jun'):
        month = 'May'
        day = 31;
        print(`${day}-${month}-${year}`);
        break;
    case (day === 0 && month === 'May'):
        month = 'Apr'
        day = 30;
        print(`${day}-${month}-${year}`);
        break;
    case (day === 0 && month === 'Apr'):
        month = 'Mar'
        day = 31;
        print(`${day}-${month}-${year}`);
        break;
    case (day === 0 && month === 'Mar' && leaping === true):
        month = 'Feb'
        day = 29;
        print(`${day}-${month}-${year}`);
        break;
    case (day === 0 && month === 'Mar' && leaping === false):
        month = 'Feb'
        day = 28;
        print(`${day}-${month}-${year}`);
        break;
    case (day === 0 && month === 'Feb'):
        month = 'Jan'
        day = 31;
        print(`${day}-${month}-${year}`);
        break;
    default:
        print(`${day}-${month}-${year}`)
}