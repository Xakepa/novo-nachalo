const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
  '-3',
  '2',
  '0.5',
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

let first = +gets();
let second = +gets();
let third = +gets();

let max = Math.max(first, second, third);
let min = Math.min(first, second, third);
let middle;

if (first < max && first > min) {
    middle = first;
} else if (second < max && second > min) {
    middle = second;
} else if (third < max && third > min) {
    middle = third;
} else if (first === third) {
    middle = first;
} else if (first === second) {
    middle = second;
} else if (first === third) {
    middle = third;
} else if (second === third) {
    middle = third;
}
print(max, middle, min);