const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
  'ku4ka'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const mixed = gets();
const numberMatch = /[0-9]*/g;
const nums = mixed.match(numberMatch);
let digits = nums.join('').split('').map(Number)
let sum = 0;

for (const num of digits) {
    sum += num;
}

if (digits.length) {
    print(sum)
} else {
    print(-1)
}
