const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '10'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const n = +gets();

const isPrime = function (num) {

    let prime = 1;

    for (let i = 2; i <= Math.sqrt(num); i += 1) {
        if (num % i === 0) {
            prime = 0;
            break;
        }
    }
    return prime;
}

let primeTriangle = ''


for (let z = 1; z <= n; z += 1) {
    primeTriangle += isPrime(z);
    if (isPrime(z)) {
        print(primeTriangle)
    }
}
