'use strict';

const codes = gets().split(' ').map(Number);

const mirrorForce = (str) => {
    str = str.toString();
    return str.split('').reverse().join('');
}
print(Math.max(mirrorForce(+codes[0]), mirrorForce(+codes[1])));