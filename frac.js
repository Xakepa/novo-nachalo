const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
  '4 10'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const [num1, num2] = gets().split(' ').map(Number);

const gcd = (a, b) => {
    const min = Math.min(a, b);
    let greatest;

    for (let i = 1; i <= min; i += 1) {
        if (a % i === 0 && b % i === 0) {
            greatest = i;
        }
    }
    return greatest;
}

let greatestCD = gcd(num1, num2)
print(num1 / greatestCD, num2 / greatestCD);