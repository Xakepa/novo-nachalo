'use strict'
const getGets = (arr) => {
    let index = 0

    return () => {
        const toReturn = arr[index]
        index += 1
        return toReturn
    }
}
// this is the test
const test = [
    '6 6',
    '92 11 23 42 59 48',
    '09 92 23 72 56 14',
    '17 63 92 46 85 95',
    '34 12 52 69 23 95',
    '26 88 78 71 29 95',
    '26 34 16 63 39 95',
]

const gets = this.gets || getGets(test)
const print = this.print || console.log

const [rows, cols] = gets().split(' ')
const allArrays = [];

for (let i = 0; i < rows; i++) {
    allArrays.push(...gets().split(' ').map(Number));
}
let sorted = allArrays.sort((x, z) => (x - z));
let countSeqs = 1;
let maxSeqs = 0;


for (let i = 0; i < sorted.length; i++) {
    if (sorted[i] === sorted[i + 1]) {
        countSeqs += 1;
        if (maxSeqs < countSeqs) {
            maxSeqs = countSeqs
        }
    } else {
        countSeqs = 1;
    }
}
print(maxSeqs);