const x = +gets();
const y = +gets();
let quadrant;

if (x > 0 && y > 0) {
    quadrant = 1;
} else if (x < 0 && y > 0) {
    quadrant = 2;
} else if (x < 0 && y < 0) {
    quadrant = 3;
} else if (x > 0 && y < 0) {
    quadrant = 4;
} else if (x === 0 && y > 0) {
    quadrant = 5;
} else if (x > 0 && y === 0) {
    quadrant = 6;
} else if (x === 0 && y === 0) {
    quadrant = 0;
} else if (x < 0 && y === 0) {
    quadrant = 6;
} else if (x === 0 && y < 0) {
    quadrant = 5;
}
print(quadrant)