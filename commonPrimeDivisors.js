const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '8 30'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const [m, n] = gets().split(' ').map(Number);


const commonDivisors = (x, z) => {

    const cd = [];
    const min = Math.min(x, z);
    for (let i = 2; i < min; i++) {
        if (x % i === 0 && z % i === 0) {
            cd.push(i)
        }
    }
    return cd;
}

const checkForPrime = (p) => {
    let prime = true;

    for (let i = 2; i <= Math.sqrt(p) ; i++) {
        if (!(p % i)) {
            prime = false;
            break;
        }
    }
    return prime;
}

const cd = commonDivisors(m, n);
const allPrimeCommonDivisors = [];

for (number of cd) {
    if (checkForPrime(number)) {
        allPrimeCommonDivisors.push(number);
    }
}

(allPrimeCommonDivisors.length) ? print(allPrimeCommonDivisors.join(' ')) : print(-1);