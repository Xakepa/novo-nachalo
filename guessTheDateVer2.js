const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '2004',
    '3',
    '1'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

let year = +gets();
let month = +gets();
let day = +gets();

let yesterday = new Date(year, month - 1, day - 1);
let monthS = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
print(`${yesterday.getDate()}-${monthS[yesterday.getMonth()]}-${yesterday.getFullYear()}`);