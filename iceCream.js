'use strict';
const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
   '3 1234'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const [elephants, randomNumber] = gets().split(' ');
const arrRandomNumber = Array.from(randomNumber).map(Number);
let luckers = 0;
let losers = 0;

for (const digit of arrRandomNumber) {
    if (digit !== 0) {
        luckers += 1;
    }
}
losers = elephants - luckers;

print(losers)