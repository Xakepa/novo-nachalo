const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '12ab23bc2436k345brg3345.'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const floodedDeclaration = gets();
const numbersFilter = /[0-9]*/g;

const numbers = floodedDeclaration
    .match(numbersFilter)
    .filter(x => x.length > 0 && x % 2 === 0)


if (numbers.length) {
    print(Math.max(...numbers));
} else {
    print(-1);
}