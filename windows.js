const [windows, price] = gets().split(' ').map(Number);
let totalSum = 0;

for (let i = 0; i < windows; i++) {
    const sizes = gets().split(' ').map(Number);
    if (sizes[2]) {
        totalSum += sizes[0] * sizes[1]
    }
}

totalSum *= price;
print(totalSum);