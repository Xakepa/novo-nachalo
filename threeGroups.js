const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '1 2 3 4 5 6 7'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const inputArray = gets().split(' ').map(Number);
let zeroReminder = '';
let oneReminder = '';
let twoReminder = '';

for (const num of inputArray) {
    switch (true) {
    case num % 3 === 0:
        zeroReminder += num + ' ';
        break;
    case num % 3 === 2:
        twoReminder += num + ' ';
        break;
    case num % 3 === 1:
        oneReminder += num + ' ';
    }
}

print(zeroReminder);
print(oneReminder);
print(twoReminder);