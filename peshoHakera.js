const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '15 23 2 3'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const [a, b, p1, p2] = gets().split(' ').map(Number);

const digit = (num) => {
    let undeleted = '';
    num = num.toString();
    for (const digit of num) {
        if (digit % p1 && digit % p2) {
            undeleted += digit;
        }
    }
    return undeleted;
}
let countLength = 0;

for (let i = a; i <= b; i += 1) {
    countLength += digit(i).length;
}
print(countLength);