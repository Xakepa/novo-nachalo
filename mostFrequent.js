const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '13',
    '4',
    '1',
    '1',
    '4',
    '2',
    '3',
    '4',
    '4',
    '1',
    '2',
    '4',
    '9',
    '3'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const length = +gets();
const list = Array.from({
    length: length
}).map(() => +gets());

list.sort((a, b) => (a - b))

let current = 1;
let max = 0;
let num = 0;

list.forEach((_, i) => {
    if (list[i] === list[i + 1]) {
        current += 1;
    } else {
        current = 1;
    }
    if (max < current) {
        max = current;
        num = list[i];
    }
});
print(`${num} (${max} times)`);