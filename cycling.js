const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '3',
    '20 15 400',
    '10 30 320',
    '5 0 40'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const days = +gets();
let maxSpeedDay = 0;
let maxSpeed = 0;
let totalMinutes = 0;

for (let i = 1; i <= days; i += 1) {
    const [hours, minutes, km] = gets().split(' ').map(Number);
    const theSpeed = (km * 1000) / ((hours * 60) + minutes);
    totalMinutes += (hours * 60) + minutes
    if (maxSpeed < theSpeed) {
        maxSpeed = theSpeed;
        maxSpeedDay = i;
    }
};

const day = (totalMinutes / 60) / 24 | 0;
const hour = (totalMinutes / 60 | 0) - 24 * day;
const minute = totalMinutes % 60;

print(day, hour, minute, maxSpeedDay);