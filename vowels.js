'use strict';

const word = gets();
let vowel = 0;
let nonVowel = 0;

for (const letter of word) {

    if (letter === 'a' || letter === 'o' || letter === 'e' ||
        letter === 'u' || letter === 'i' || letter === 'y') {
        vowel += 1;
    } else {
        nonVowel += 1;
    }
}
(vowel === nonVowel) ? print('Yes') : print(vowel, nonVowel);