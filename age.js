const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '09.05.2016'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const [month, day, year] = gets().split('.').map(Number);
const currentBirthday = new Date(year, month - 1, day - 1);
const currentDay = new Date();
const currentAge = currentDay.getFullYear() - currentBirthday.getFullYear();
print(currentAge);
print(currentAge + 10);