const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '0',
    '4',
    '20',
    '1337',
    '2147483648',
    '4000000000',
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const binDigit = gets();
const size = +gets();
const allNumbs = Array.from({
    length: size
}).map(() => +gets());


for (const num of allNumbs) {
    let binary = num.toString(2);
    let found = binary.split('').filter(a => a == binDigit);
    print(found.length);
}