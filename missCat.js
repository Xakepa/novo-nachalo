const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '4',
    '1',
    '3',
    '3',
    '7',
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const jury = +gets();
const votes = Array.from({
    length: jury
}).map(() => +gets()).sort((a, b) => (a - b));

let maxVotes = 0;
let catNumber = 0;

const result = votes.reduce(((all, elem) => {

    if (!all[elem]) {
        all[elem] = 0;
    }
    all[elem]++
    if (all[elem] > maxVotes) {
        maxVotes = all[elem];
        catNumber = elem;
    }
    return all;
}), {});

if (!(maxVotes)) {
    print(votes[0]);
} else {
    print(catNumber);
};