const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '9',
    '-1',
     '0',
    '1',
    '2',
    '3',
    '2',
    '1',
    '0',
    '-1'

];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const length = +gets();
const list = Array.from({
    length: length
}).map(() => gets()).sort((a, b) => (a - b));

let currentLength = 1;

list.forEach((x, i) => {
    if (x === list[i + 1]) {
        currentLength += 1;
    } else {
        if (currentLength % 2) {
            print(x);
            return;
        }
        currentLength = 1;
    }
});