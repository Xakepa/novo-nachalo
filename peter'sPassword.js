const [password, key] = gets().split(' ');
const reversed = password.split('').reverse().join('')
const remainder = +reversed % +key;

print(reversed / key + remainder | 0);