const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    'A'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

let randomNumberOfDeck = gets();
const deck = [null, null, 2, 3, 4, 5, 6, 7, 8, 9, 10, 'J', 'Q', 'K', 'A'];
if (isNaN(+randomNumberOfDeck)) {
    randomNumberOfDeck = deck.indexOf(randomNumberOfDeck)
}

for (let i = 0; i <= +randomNumberOfDeck; i += 1) {
    if (deck[i]) {
        print(`${deck[i]} of spades, ${deck[i]} of clubs, ${deck[i]} of hearts, ${deck[i]} of diamonds`);
    }
}