const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
  '5',
  '4 3 2 5 2',
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const nSize = +gets();
const products = gets().split(' ').map(Number);
let sumEven = 1;
let sumOdd = 1;

for (let i = 0, j = i + 1; i < products.length; i += 2, j += 2) {
    sumEven *= products[i];
    if (products[j]) {
        sumOdd *= products[j];
    }
}
(sumEven === sumOdd) ? print(`yes ${sumEven}`) : print(`no ${sumEven} ${sumOdd}`);