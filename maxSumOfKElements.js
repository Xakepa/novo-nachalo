const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '10',
    '4',
    '3',
    '0',
    '-34',
    '2',
    '3',
    '-2',
    '5',
    '4',
    '2',
    '7',
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const nLength = +gets();
const k = +gets();
const nArray = [];

for (let i = 0; i < nLength; i += 1) {
    nArray.push(+gets())
}
nArray.sort((x, z) => (x - z)).reverse();

let maxSumOfK = 0;

for (let z = 0; z < k; z += 1) {
    maxSumOfK += nArray[z];
}
print(maxSumOfK);