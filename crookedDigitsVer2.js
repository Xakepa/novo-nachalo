let number = gets();

while (number.length > 1) {
    let newNumber = 0;

    for (let ch of number) {
        if (isNaN(+ch)) {
            continue;
        }
        newNumber += +ch;
    }
    number = newNumber.toString();
}
print(number);