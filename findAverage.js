const getGets = (arr) => {
    let index = 0

    return () => {
        const toReturn = arr[index]
        index += 1
        return toReturn
    }
}
// this is the test
const test = [
    '3',
    '2.5',
    '1.25',
    '3'
]

const gets = this.gets || getGets(test)
const print = this.print || console.log

const collectionSize = +gets()
const collection = Array.from({
    length: collectionSize
}).map(() => +gets())

const total = collection.reduce((acc, sum) => acc + sum, 0)
print((total / collectionSize).toFixed(2))