const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
  '10'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const n = +gets();
let startIndex = 0;

const divisable = (x) => {
    return (x % 3 < 1 || x % 7 < 1) ? true : false
}


let singleLine = '';
while (startIndex < n) {
    startIndex += 1;
    if (!(divisable(startIndex))) {
        singleLine += startIndex + ' ';
    }
}
print(singleLine);