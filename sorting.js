'use strict';

const pots = gets().split(' ').map(Number);
let ascending = 1;
let descending = 1;

for (let i = 0; i < pots.length; i++) {
    if (pots[i] < pots[i + 1]) {
        ascending += 1;
    }
    if (pots[i] > pots[i + 1]) {
        descending += 1;
    }
}
if (pots.length === ascending) {
    print('Ascending')
} else if (pots.length === descending) {
    print('Descending')
} else {
    print('Mixed')
}