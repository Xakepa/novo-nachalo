const getGets = (arr) => {
    let index = 0

    return () => {
        const toReturn = arr[index]
        index += 1
        return toReturn
    }
}
// this is the test
const test = [
    '7',
    'P4 P2 P3 M1 K2 P1 K1',
]

const gets = this.gets || getGets(test)
const print = this.print || console.log

const jediCount = +gets();
const allJedi = gets().split(' ');
const masters = [];
const knights = [];
const padawans = [];

for (const jedi of allJedi) {
    if (jedi[0] === 'M') {
        masters.push(jedi);
    } else if (jedi[0] === 'K') {
        knights.push(jedi)
    } else if (jedi[0] === 'P') {
        padawans.push(jedi);
    }
}
print(masters.concat(knights, padawans).join(' '));