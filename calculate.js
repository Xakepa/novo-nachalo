const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '4',
    '3',
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const n = +gets();
const x = +gets();

const factorial = (x) => {
    let f = 1;
    for (let i = 1; i <= x; i += 1) {
        f *= i;
    }
    return f;
}
let result = 1;

for (let i = 1; i <= n; i += 1) {
    result += factorial(i) / Math.pow(x, i) 
}
print(result.toFixed(5));