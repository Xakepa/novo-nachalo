const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
     'real',
    '3.4'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const type = gets();
let value = gets();

switch (type) {
    case 'integer': +value++;
    break;
    case 'real': value = (+value + 1).toFixed(2);
    break;
    case 'text': value += '*';
}
print(value);