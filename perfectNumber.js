// In number theory, a perfect number is a positive
//  integer that is equal to the sum of its proper positive 
// divisors, that is, the sum of its positive divisors 
// excluding the number itself (also known as its aliquot sum). 
// Equivalently, a perfect number is a number that is half
// the sum of all of its positive divisors (including itself).

const perfectNumber = function (n) {

    let perfect = false;
    let divisors = 1;

    for (let i = 2; i <= n / 2; i++) {
        if (n % i === 0) {
            divisors += i;
        }
    }
    if (divisors === n) {
        perfect = true;
    }
    return perfect;
}
for (let i = 2; i < 10000; i++) {
    if (perfectNumber(i)) {
        console.log(i);
    }
}