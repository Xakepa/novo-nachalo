const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    'set 2',
    'print',
    'front-add 1',
    'print',
    'back-add 2',
    'print',
    'front-remove',
    'print',
    'set 4',
    'print',
    'print',
    'front-add 1',
    'print',
    'back-add 3',
    'print',
    'reverse',
    'print',
    'end'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

let str = gets();
let currentValue = '';

while (str !== 'end') {

    switch (true) {
    case (str.slice(0, 3) === 'set'):
        currentValue = str.slice(4);
        break;
    case (str.slice(0, 9) === 'front-add'):
        currentValue = str.slice(10) + currentValue;
        break;
    case (str === 'print'):
        print(currentValue);
        break;
    case (str.slice(0, 8) === 'back-add'):
        currentValue = currentValue + str.slice(9);
        break;
    case (str === 'front-remove'):
        currentValue = currentValue.slice(1);
        break;
    case (str === 'back-remove'):
        currentValue = currentValue.slice(0, (currentValue.length - 1));
        break;
    case (str === 'reverse'):
        currentValue = currentValue.split('').reverse().join('');
        break;
    }
    str = gets();
}