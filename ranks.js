const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '4',
    '1 3 4 2'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const arraySize = +gets();
const theArray = gets().split(' ').map(Number);
const sorted = theArray.slice().sort((a, b) => (b - a));
const ranks = [];

for (let i = 0; i < arraySize; i++) {
    ranks.push(sorted.indexOf(theArray[i]) + 1)
}
print(ranks.join(' '));

// Option 2 with Nested Loops

// for (let i = 0; i < theArray.length; i += 1) {
//     for (let k = 0; k < theArray.length; k += 1) {
//         if (theArray[i] === sorted[k]) {
//             ranks.push(k + 1)
//         }
//     }
// }
// print(ranks.join(' '));