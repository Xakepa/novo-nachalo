const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '10',
    '2',
    '3',
    '-6',
    '-1',
    '2',
    '-1',
    '6',
    '4',
    '-8',
    '8'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const seqLength = +gets();
const sequence = Array.from({
    length: seqLength
}).map(() => (+gets()));


const sums = [];
let sum = 0;

for (let i = 0; i < sequence.length; i += 1) {
    
    if (i !== 0) {
        sum = 0;
    
        for (let z = i; z <= sequence.length; z += 1) {

            sum += sequence[z - 1];
            sums.push(sum);
        }
    }  
}
sums.sort((a, b) => (a - b));

print(sums[sums.length - 1]);