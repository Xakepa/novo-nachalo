const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '6', 
'1 1 2 5 2 6',
'7',
'6 5 1 6 2 5 6'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const nSize = +gets();
let firstArray = Array.from(gets().split(' ').map(Number));
const kSize = +gets();
let secondArray = Array.from(gets().split(' ').map(Number));

const firstSet = new Set(firstArray);
const secondSet = new Set(secondArray);
firstArray = Array.from(firstSet).sort((x, y) => (x - y));
secondArray = Array.from(secondSet).sort((x, y) => (x - y));

if (JSON.stringify(firstArray) === JSON.stringify(secondArray)) {
    print(firstArray.length);
} else {
    let filtered = firstArray.filter((unique) => !secondArray.includes(unique));
    print(filtered.join(' '));
}