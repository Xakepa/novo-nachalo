const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '126'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const isPrime = (num) => {

    let prime = true;

    for (let i = 2; i < num; i += 1) {
        if (num % i === 0) {
            prime = false;
            break;
        }
    }
    return prime;
}

let inputNumber = +gets();

while (!isPrime(inputNumber)) {
    inputNumber -= 1;
}
print(inputNumber);