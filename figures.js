'use strict';
const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '2 2 2 2 2'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const [a, b, c, d, h] = gets().split(' ').map(Number);
const maxTriangleSide = Math.max(a, b, c);
const maxRectangleSide = Math.max(d, h);
const breakPoint = Math.min(maxTriangleSide, maxRectangleSide) * 2;
const band = (d * 2 + h * 2) + a + b + c - breakPoint;

print(band);