#include <iostream>
#include <string>
using namespace std;
int main()
{
    int a, b, bigger, smaller;
    cin >> a >> b;

    if (a > b)
    {
        bigger = a;
        smaller = b;
    }
    else
    {
        bigger = b;
        smaller = a;
    }

    cout << bigger - smaller << endl;
    return 0;
}
