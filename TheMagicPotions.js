const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '10 5'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const [magicPotions, elexirAmount] = gets().split(' ').map(Number);
let quantity = 0;

for (let i = 1; i <= magicPotions; i += 1) {
    quantity += i - 1;
    if (quantity >= elexirAmount && i % 2 !== 0) {
        print(i);
        break;
    }  
}